from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from cooking.views import homepage_view, roulette, upload, del_all, main, \
    receipt, admin_tools, scrap, add_receipt_form, delete_rec, update_rec, \
    add_ingredients, update_rec_form, registred


urlpatterns = [
    path('', homepage_view, name='homepage'),
    path('accounts/', include("django.contrib.auth.urls")),
    path('admin/', admin.site.urls),
    path('roulette/', roulette, name='roulette'),
    path('upload/', upload, name='upload'),
    path('del_all/', del_all, name='del_all'),
    path('admin_tools/', admin_tools, name='admin_tools'),
    path('scrap/', scrap, name='scrap'),
    path('add_receipt_form/', add_receipt_form, name='add_receipt_form'),
    path('delete_rec/<int:id>', delete_rec, name='delete_rec'),
    path('update_rec/<int:id>', update_rec, name='update_rec'),
    path('add_ingredients/<int:id>', add_ingredients, name='add_ingredients'),
    path('update_rec_form/<int:id>', update_rec_form, name='update_rec_form'),
    path('registred', registred, name='registred')
]